from typing import Any, Dict, Generator, Union

from unittest.mock import MagicMock, patch

import pytest

from minimalmodbus import ModbusException

from hanmatek import HM3xxP
from hanmatek.HM3xxP_regmap import REGMAP, RegSpec


@pytest.fixture
def InstrumentClass() -> Generator[MagicMock, None, None]:
    with patch("minimalmodbus.Instrument") as instr:
        yield instr


@pytest.fixture
def device(InstrumentClass: MagicMock) -> Generator[HM3xxP, None, None]:
    with InstrumentClass:
        yield HM3xxP(port="/dev/test", slave_addr=1)


@pytest.mark.parametrize(
    "value",
    [
        "1",
        1,
        1.0,
        "1.0",
        "yes",
        "YES",
        "Yes",
        "true",
        "TRUE",
        "True",
        True,
        "on",
        "ON",
        "On",
    ],
)
def test_boolish_true(value: Any) -> None:
    assert HM3xxP._boolish(value) is True


@pytest.mark.parametrize(
    "value",
    [
        "0",
        0,
        0.0,
        "0.0",
        "no",
        "NO",
        "No",
        "false",
        "FALSE",
        "False",
        False,
        "off",
        "OFF",
        "Off",
    ],
)
def test_boolish_false(value: Any) -> None:
    assert HM3xxP._boolish(value) is False


def test_discover(InstrumentClass: MagicMock) -> None:

    device_tree: Dict[str, Any] = {
        "/dev/a": {
            # no device
        },
        "/dev/b": {
            1: {  # matching model, wrong class
                "model": 205,
                "class": 19280,
            },
            2: {  # matching class, wrong model
                "model": 305,
                "class": 19281,
            },
            3: {"model": 305},  # matching model, no class
            4: {"class": 19280},  # matching class, no model
        },
        "/dev/c": {
            1: {  # both not matching
                "model": 306,
                "class": 19281,
            },
            2: {  # match!
                "model": 305,
                "class": 19280,
            },
        },
        "/dev/d": {
            1: {  # match!
                "model": 305,
                "class": 19280,
            },
        },
        "/dev/e": {
            2: {  # match!
                "model": 305,
                "class": 19280,
            },
            4: {  # match!
                "model": 3010,
                "class": 19280,
            },
        },
    }

    addr_map = {
        0x0003: "model",
        0x0004: "class",
    }

    def instrument_constructor(*args: Any, **kwargs: Any) -> MagicMock:
        port = kwargs.pop("port")
        slave_addr = kwargs.pop("slaveaddress")

        def read_func(addr: int, decimals: int) -> int:
            if slave_addr in device_tree[port]:
                if addr_map[addr] in device_tree[port][slave_addr]:
                    return int(device_tree[port][slave_addr][addr_map[addr]])
            raise ModbusException()

        instance = MagicMock()
        instance.read_register = read_func
        return instance

    InstrumentClass.side_effect = instrument_constructor
    devices = HM3xxP.discover(ports=list(device_tree.keys()), addresses=list(range(0, 5)))

    devices_ids = [f"{device.port}:{device.slave_addr}" for device in devices]

    assert len(devices_ids) == 4

    assert "/dev/c:2" in devices_ids
    assert "/dev/d:1" in devices_ids
    assert "/dev/e:2" in devices_ids
    assert "/dev/e:4" in devices_ids


@pytest.mark.parametrize("value", ["2", 2, 1.5, "bad", "good", "Truee"])
def test_boolish_error(value: Any) -> None:
    with pytest.raises(ValueError) as exc_info:
        HM3xxP._boolish(value)
    assert "cannot convert" in str(exc_info.value)


@pytest.mark.parametrize("register", [None, "", "unknown"])
def test_read_unknown_register(device: HM3xxP, register: str) -> None:
    with pytest.raises(ValueError) as exc_info:
        device.read(register=register)
    assert "unknown register" in str(exc_info.value)


@pytest.mark.parametrize("register", [None, "", "unknown"])
def test_write_unknown_register(device: HM3xxP, register: str) -> None:
    with pytest.raises(ValueError) as exc_info:
        device.write(register=register, value=0)
    assert "unknown register" in str(exc_info.value)


def test_read_write_only_register(device: HM3xxP) -> None:
    REGMAP["write-only"] = RegSpec(bool, "w", 0x0000)
    with pytest.raises(ValueError) as exc_info:
        device.read(register="write-only")
    assert 'not a "read" register' in str(exc_info.value)
    del REGMAP["write-only"]


def test_write_read_only_register(device: HM3xxP) -> None:
    with pytest.raises(ValueError) as exc_info:
        device.write(register="model", value=0)
    assert 'not a "write" register' in str(exc_info.value)


def test_read_unsupported_dtype(device: HM3xxP) -> None:
    REGMAP["unsupported-dtype"] = RegSpec(dtype=int, rw="r", addr=0x0000)
    REGMAP["unsupported-dtype"].dtype = str  # type: ignore

    with pytest.raises(AssertionError) as exc_info:
        device.read("unsupported-dtype")
    assert "unsupported data type" in str(exc_info.value)

    del REGMAP["unsupported-dtype"]


def test_write_unsupported_dtype(device: HM3xxP) -> None:
    REGMAP["unsupported-dtype"] = RegSpec(dtype=int, rw="w", addr=0x0000)
    REGMAP["unsupported-dtype"].dtype = str  # type: ignore

    with pytest.raises(AssertionError) as exc_info:
        device.write("unsupported-dtype", 0)
    assert "unsupported data type" in str(exc_info.value)

    del REGMAP["unsupported-dtype"]


def test_read_number(InstrumentClass: MagicMock, device: HM3xxP) -> None:

    for regname in REGMAP:

        if REGMAP[regname].dtype == bool:
            continue

        InstrumentClass().read_register.return_value = REGMAP[regname].addr
        InstrumentClass().read_long.return_value = REGMAP[regname].addr

        assert device.read(regname) == pytest.approx(
            REGMAP[regname].addr / REGMAP[regname].multiplier
        ), f"register {regname}"


def test_read_bool(InstrumentClass: MagicMock, device: HM3xxP) -> None:

    for regname in REGMAP:

        if REGMAP[regname].dtype != bool:
            continue

        InstrumentClass().read_register.return_value = int(REGMAP[regname].addr % 2 == 0)

        assert device.read(regname) is (REGMAP[regname].addr % 2 == 0), f"regname: {regname}"


def test_write_number(InstrumentClass: MagicMock, device: HM3xxP) -> None:

    written_value = None

    def write_number(addr: int, value: Union[int, float], decimals: int = 0) -> None:
        nonlocal written_value
        assert type(value) in [int, float]
        written_value = value

    for regname in REGMAP:

        if REGMAP[regname].dtype == bool:
            continue
        if "w" not in REGMAP[regname].rw:
            continue

        InstrumentClass().write_register = write_number
        InstrumentClass().write_long = write_number

        device.write(regname, REGMAP[regname].addr)

        assert written_value == pytest.approx(
            REGMAP[regname].addr * REGMAP[regname].multiplier
        ), f"register {regname}"


def test_write_bool(InstrumentClass: MagicMock, device: HM3xxP) -> None:

    written_value = None

    def write_bool(addr: int, value: int, decimals: int = 0) -> None:
        nonlocal written_value
        assert type(value) == int
        written_value = value

    for regname in REGMAP:

        if REGMAP[regname].dtype != bool:
            continue
        if "w" not in REGMAP[regname].rw:
            continue

        InstrumentClass().write_register = write_bool

        device.write(regname, REGMAP[regname].addr % 2 == 0)

        assert written_value == int(REGMAP[regname].addr % 2 == 0), f"register {regname}"


def test_info(InstrumentClass: MagicMock, device: HM3xxP) -> None:
    InstrumentClass().read_register.side_effect = [19280, 305]
    info = device.info()
    assert info == "KP305"
